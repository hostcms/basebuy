<?php
use Utils_Utl as utl;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 31.08.2017
 * Time: 14:48
 */
class Basebuy_Moto_Entity extends Core_Entity {
	protected $_addBaseQuery;
	protected $_pathFields = array();
	public $largeImageMaxWidth = 2000;
	public $largeImageMaxHeight = 2000;

	/**
	 * Basebuy_Moto_Entity constructor.
	 * @param $_baseQuery
	 */
	public function __construct($primaryKey = NULL)
	{
		$this->_addBaseQuery = false;

		parent::__construct($primaryKey);
	}

	/**
	 * @return bool
	 */
	public function isAddBaseQuery()
	{
		return $this->_addBaseQuery;
	}

	/**
	 * @param bool $addBaseQuery
	 */
	public function addBaseQuery()
	{
		$this->_addBaseQuery = true;
		return $this;
	}

	public function getCurrentModelName() {
		$names = array();
		preg_match('/(^Basebuy\_.*)\_Model$/', get_class($this), $names);
		return $names[1];
	}

	public function getModelShortName() {
		$names = array();
		preg_match('/^basebuy\_(.*)\_model$/', strtolower(get_class($this)), $names);
		return $names[1];
	}

	/**
	 * Get item large image path
	 * @return string
	 */
	public function getLargeFilePath()
	{
		return $this->getItemPath() . $this->image_large;
	}

	/**
	 * Get item large image href
	 * @return string
	 */
	public function getLargeFileHref()
	{
		return $this->getItemHref() . rawurlencode($this->image_large);
	}

	/**
	 * Get item path include CMS_FOLDER
	 * @return string
	 */
	public function getItemPath()
	{
		return str_replace('//', '/', CMS_FOLDER . $this->getItemHref());
	}

	/**
	 * Get item href
	 * @return string
	 */
	public function getItemHref()
	{
		return '/upload/basebuy/' . Core_File::getNestingDirPath($this->getPrimaryKey(), 3) . '/'.$this->getModelShortName() . '/';
	}


	/**
	 * Delete item's large image
	 * @return self
	 */
	public function deleteLargeImage()
	{
		$fileName = $this->getLargeFilePath();
		if ($this->image_large != '' && is_file($fileName))
		{
			try
			{
				Core_File::delete($fileName);
			} catch (Exception $e) {}

			$this->image_large = '';
			$this->save();
		}
		return $this;
	}

	/**
	 * Create directory for item
	 * @return self
	 */
	public function createDir()
	{
		clearstatcache();

		if (!is_dir($this->getItemPath()))
		{
			try
			{
				Core_File::mkdir($this->getItemPath(), CHMOD, TRUE);
			} catch (Exception $e) {}
		}

		return $this;
	}

	/**
	 * Check and correct duplicate path
	 * @return self
	 */
	public function checkDuplicatePath($exception=true)
	{
		foreach ($this->_pathFields as $pathModel => $pathField) {
			$mSameShopItem = Core_Entity::factory($pathModel);
			if(!is_array($pathField)) {
				$pathField = array($pathField);
			}
			foreach ($pathField as $pField) {
				$mSameShopItem
					->queryBuilder()
					->where($this->getTableName().'.'.$pField, '=', $this->$pField)
				;
			}
			if($this->getPrimaryKey()>0) {
				$mSameShopItem
					->queryBuilder()
					->where($this->getTableName().'.'.$this->getPrimaryKeyName(), '<>', $this->getPrimaryKey())
				;
			}
			$oSameShopItem = $mSameShopItem->getByPath($this->path, false);

			if(!is_null($oSameShopItem)) {
				if( $exception) {
					throw new Core_Exception('Дублирование пути в каталоге', array(), 3, FALSE);
				} else {
					$this->path = Core_Guid::get();
				}
			}
		}
		return $this;
	}

	/**
	 * Make url path
	 * @return self
	 */
	public function makePath()
	{
		try {
			Core::$mainConfig['translate'] && $sTranslated = Core_Str::translate($this->name);

			$this->path = Core::$mainConfig['translate'] && strlen($sTranslated)
				? $sTranslated
				: $this->name;

			$this->path = Core_Str::transliteration($this->path);

		} catch (Exception $e) {
			$this->path = Core_Str::transliteration($this->name);
		}

		$this->checkDuplicatePath(false);

		return $this;
	}
//
//	/**
//	 * Save object. Uses self::update() or self::create()
//	 *
//	 * @return Core_ORM
//	 * @hostcms-event modelname.onBeforeSave
//	 * @hostcms-event modelname.onAfterSave
//	 */
//	public function save()
//	{
//		return parent::save();
//	}


	public function getXml()
	{
		if(isset($this->image_large)) {
			//		if(isset($this->image_large) && $this->image_large=='') {
			$entities = $this->getEntities();
			$dirNotExist = true;
			foreach ($entities as $entity) {
				if($entity->name=='image_src') {
					$dirNotExist = false;
					break;
				}
//				Utils_Utl::p($entity->name);
			}
////			Utils_Utl::p( $this->getEntities());
////			$this
////				->addEntity(
////					Core::factory('Core_Xml_Entity')
////						->name('image_large')
////						->value($this->moto_model->image_large)
////				)
////				->addEntity(
////					Core::factory('Core_Xml_Entity')
////						->name('dir')
////						->value($this->moto_model->getItemHref())
////				)
////			;
//		}

			if($dirNotExist && $this->image_large!='') {
				$this
					->addEntity(
						Core::factory('Core_Xml_Entity')
							->name('image_src')
							->value($this->getItemHref().$this->image_large)
					)
				;
			}
			$this
				->addEntity(
					Core::factory('Core_Xml_Entity')
						->name('dir')
						->value($this->getItemHref())
				)
			;
		}
		return parent::getXml(); // TODO: Change the autogenerated stub
	}

}