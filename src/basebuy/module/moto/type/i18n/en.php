<?php
/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
return array(
	'menu' => 'Каталог мототехники. Типы',
	'header_admin_forms' => 'Действия с типами техники',
	'menu_add' => 'Добавить тип',
	'name' => 'Тип мототехники',
	'edit_success' => 'Выполнено успешно',
	'edit_title' => 'Редактировать тип мототехники',
	'add_title' => 'Добавить тип мототехники',
	'alone_rus' => 'Наименование в единственном числе',
	'alone_en' => 'Наименование по-английски в единственном числе',
	'path' => 'Локальный путь',
	'id_car_type' => 'Идентификатор',
	'shop_group_id' => 'Связка с группой ИМ',
);