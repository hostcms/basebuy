<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
class Basebuy_Moto_Mark_Controller_Edit extends Basebuy_Moto_Controller_Edit
{

	protected function _prepareForm()
	{
		parent::_prepareForm();

		$object = $this->_object;

//		$modelName = $object->getModelName();
//
//		$oMainTab = $this->getTab('main');
//		$oAdditionalTab = $this->getTab('additional');

		$title = $object->id_car_type
			? Core::_('Basebuy_Moto_Mark.edit_title')
			: Core::_('Basebuy_Moto_Mark.add_title');

		/** @var Admin_Form_Entity $typeField */
		$typeField = $this->getField('id_car_mark');

		$typeField->readonly('readonly');

		$this->title($title);

		return $this;
	}
}