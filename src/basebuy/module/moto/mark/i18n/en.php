<?php
/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
return array(
	'menu' => 'Каталог мототехники. Марки',
	'header_admin_forms' => 'Действия с марками техники',
	'menu_add' => 'Добавить марку',
	'edit_title' => 'Редактировать марку мототехники',
	'add_title' => 'Добавить марку мототехники',
	'edit_success' => 'Выполнено успешно',
	'name' => 'Марка мототехники',
	'name_rus' => 'Наименование в единственном числе',
	'path' => 'Локальный путь',
);