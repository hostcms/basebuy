<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
class Basebuy_Moto_Model_Controller_Edit extends Basebuy_Moto_Controller_Edit
{
//	/**
//	 * Set object
//	 * @param object $object object
//	 * @return self
//	 */
//	public function setObject($object)
//	{
//		// Пропускаем поля, обработка которых будет вестись вручную ниже
//		$this
//			->addSkipColumn('image_large')
//		;
//		$primaryKeyName = $object->getPrimaryKeyName();
//
//		if (!$object->$primaryKeyName)
//		{
//			$object->id_car_mark = Core_Array::getGet('id_car_mark');
//			$object->id_car_type = Core_Array::getGet('id_car_type');
//		}
//
//		parent::setObject($object);
//
//		return $this;
//	}

	protected function _prepareForm()
	{
		parent::_prepareForm();

		$object = $this->_object;

		$modelName = $object->getModelName();
		$primaryKeyName = $object->getPrimaryKeyName();

		$oMainTab = $this->getTab('main');
		$oAdditionalTab = $this->getTab('additional');

		$title = $object->id_car_type
			? Core::_('Basebuy_Moto_Model.edit_title')
			: Core::_('Basebuy_Moto_Model.add_title');

//		/** @var Admin_Form_Entity $typeField */
//		$typeField = $this->getField('id_car_model');
//		$typeField->readonly('readonly');

		$this->title($title);

		return $this;
	}

//	protected function _applyObjectProperty()
//	{
//		parent::_applyObjectProperty();

//		$aCore_Config = Core::$mainConfig;
//
//		$bLargeImageIsCorrect =
//			// Поле файла большого изображения существует
//			!is_null($aFileData = Core_Array::getFiles('image', NULL))
//			// и передан файл
//			&& intval($aFileData['size']) > 0;
//
//		if ($bLargeImageIsCorrect)
//		{
//			// Проверка на допустимый тип файла
//			if (Core_File::isValidExtension($aFileData['name'], $aCore_Config['availableExtension']))
//			{
//				// Удаление файла большого изображения
//				if ($this->_object->image_large)
//				{
//					// !! дописать метод
//					$this->_object->deleteLargeImage();
//				}
//
//				$file_name = $aFileData['name'];
//
//				// Определяем расширение файла
//				$ext = Core_File::getExtension($aFileData['name']);
//				$large_image = $this->_object->getModelShortName() . '_' . $this->_object->getPrimaryKey() . '.' . $ext;
//			}
//			else
//			{
//				$this->addMessage(Core_Message::get(Core::_('Core.extension_does_not_allow', Core_File::getExtension($aFileData['name'])), 'error'));
//			}
//
//			// Путь к файлу-источнику большого изображения;
//			$param['large_image_source'] = $aFileData['tmp_name'];
//			// Оригинальное имя файла большого изображения
//			$param['large_image_name'] = $aFileData['name'];
//			$param['large_image_target'] = $this->_object->getItemPath() . $large_image;
//			$param['create_small_image_from_large'] = false;
//			$param['large_image_max_width'] = 1000;
//			$param['large_image_max_height'] = 1000;
//			$this->_object->createDir();
//
//			$result = Core_File::adminUpload($param);
//
//			if ($result['large_image'])
//			{
//				$this->_object->image_large = $large_image;
//			}
//
//		}

//		$this->_object->save();
//	}
}