<?php
/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
return array(
	'menu' => 'Каталог мототехники. Модели',
	'header_admin_forms' => 'Действия с моделями техники',
	'menu_add' => 'Добавить модель',
	'edit_title' => 'Редактировать модель мототехники',
	'add_title' => 'Добавить модель мототехники',
	'edit_success' => 'Выполнено успешно',
	'name' => 'Наименование',
	'name_rus' => 'Наименование в единственном числе',
	'path' => 'Локальный путь',
);