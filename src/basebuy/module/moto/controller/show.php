<?php
use Utils_Utl as utl;

class Basebuy_Moto_Controller_Show extends Core_Controller {
	protected $_instance;

	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'type',
		'mark',
		'model',
		'modification',
		'models',
		'limit',
	);

	/**
	 * Constructor.
	 * @param Shop_Model $oShop shop
	 */
	public function __construct(Structure_Model $oStructure)
	{
		parent::__construct($oStructure->clearEntities());

		$this->_instance = Core_Page::instance();
		$this->limit = 1000;
	}

	public function parseUrl() {
		$paths = explode('/', Core_Str::ltrimUri(Core_Str::rtrimUri(Core::$url['path'])));

		if($this->_entity->path != '/') {
			unset($paths[0]);
			$paths = array_values($paths);
		}
//
		if(isset($paths[0])) {
			$type = Core_Entity::factory('Basebuy_Moto_Type');
			$vType = $type->getByPath($paths[0]);
			if(!is_null($vType)) {
				$this->type = $vType;
			} else {
				return $this->error404();
			}
		}
		if(isset($paths[1])) {
			$mark = Core_Entity::factory('Basebuy_Moto_Mark');
			$mark
				->addBaseQuery()
				->queryBuilder()
				->where('car_mark.id_car_type', '=', $this->type->id_car_type)
			;
			$vMark = $mark->getByPath($paths[1]);
			if(!is_null($vMark)) {
				$this->mark = $vMark;
			} else {
				return $this->error404();
			}
		}
		if(isset($paths[2])) {
			$model = Core_Entity::factory('Basebuy_Moto_Model');
			$model
				->addBaseQuery()
				->queryBuilder()
				->where('car_model.id_car_mark', '=', $this->mark->id_car_mark)
			;
			$vModel = $model->getByPath($paths[2]);
			if(!is_null($vModel)) {
				$this->model = $vModel;
			} else {
				return $this->error404();
			}
		}
		if(isset($paths[3])) {
			$modification = Core_Entity::factory('Basebuy_Moto_Modification');
			$modification
				->addBaseQuery()
				->queryBuilder()
				->where('car_modification.id_car_model', '=', $this->model->id_car_model)
			;
			$vModification = $modification->getByPath($paths[3]);
			if(!is_null($vModification)) {
				$this->modification = $vModification;
			} else {
				return $this->error404();
			}
		}
//		if(isset($paths[2])) {
//
//		}
//		list($this->_mark, $this->_model) = $paths;
//		utl::p($this->_mark);

		return $this;
	}

	public function show()
	{
		$oCarMarks = NULL;
		$oCarModels = NULL;

		$mCarTypes = Core_Entity::factory('Basebuy_Moto_Type');
		$oCarTypes = $mCarTypes->findAll(false);

		$mCarModifications = Core_Entity::factory('Basebuy_Moto_Modification');
		$mCarModifications
			->addBaseQuery()
			->queryBuilder()
			->clearOrderBy()
			->orderBy('car_modification.shop_item_id', 'desc')
			->limit($this->limit)
		;

		!is_array($oCarTypes) && $oCarTypes = array();
		foreach ($oCarTypes as &$oCarType) {
			$oCarType->showGroups(true);
			if(isset($this->type->id_car_type) && $this->type->id_car_type==$oCarType->id_car_type) {
				$oCarType->addEntity(Core::factory('Core_Xml_Entity')->name('active')->value(1));
			}
		}
		if(!is_null($this->type)) {
			$mCarModifications
				->queryBuilder()
				->where('car_type.id_car_type', '=', $this->type->id_car_type)
			;
			$this->addEntity($this->type);

			$oCarMarks = $this->type->moto_marks->findAll(false);
		}
		!is_array($oCarMarks) && $oCarMarks = array();
		foreach ($oCarMarks as &$oCarMark) {
			$oCarMark->showGroups(true);
			if (isset($this->mark->id_car_mark) && $this->mark->id_car_mark == $oCarMark->id_car_mark) {
				$oCarMark->addEntity(Core::factory('Core_Xml_Entity')->name('active')->value(1));
			}
		}
		if(!is_null($this->mark)) {
			$mCarModifications
				->queryBuilder()
				->where('car_mark.id_car_mark', '=', $this->mark->id_car_mark)
			;
			$this->addEntity($this->mark);

			$oCarModels = $this->mark->moto_models->findAll(false);
		}
		!is_array($oCarModels) && $oCarModels = array();
		foreach ($oCarModels as &$oCarModel) {
			$oCarModel->showItems(true);
			if (isset($this->model->id_car_model) && $this->model->id_car_model == $oCarModel->id_car_model) {
				$oCarModel->addEntity(Core::factory('Core_Xml_Entity')->name('active')->value(1));
			}
		}
		if(!is_null($this->model)) {
			$mCarModifications
				->queryBuilder()
				->where('car_model.id_car_model', '=', $this->model->id_car_model)
			;
			$this->addEntity($this->model);
		}
		if(!is_null($this->modification)) {
			$mCarModifications
				->queryBuilder()
				->where('id_car_modification', '=', $this->modification->id_car_modification)
			;
			$this->addEntity($this->modification);
		}

		$oCarModifications = $mCarModifications->findAll();
		foreach ($oCarModifications as $oCarModification) {
			$oCarModification->showItems(true);
		}

		is_array($oCarTypes) && $this
			->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('type')
					->addEntities($oCarTypes)
			);
		is_array($oCarMarks) && $this
			->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('mark')
					->addEntities($oCarMarks)
			);
		is_array($oCarModels) && $this
			->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('model')
					->addEntities($oCarModels)
			);
		is_array($oCarModifications) && $this
			->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('modification')
					->addEntities($oCarModifications)
			);
		$user = Core_Entity::factory('User')->getCurrent();
		(!is_null($user)) && $this->addEntity($user);

		return parent::show(); // TODO: Change the autogenerated stub
	}


	/**
	 * Define handler for 404 error
	 * @return self
	 */
	public function error404()
	{
		Core_Page::instance()->error404();

		return $this;
	}
}
