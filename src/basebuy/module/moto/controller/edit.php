<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
class Basebuy_Moto_Controller_Edit extends Admin_Form_Action_Controller_Type_Edit
{
	/**
	 * Set object
	 * @param object $object object
	 * @return self
	 */
	public function setObject($object)
	{
		// Пропускаем поля, обработка которых будет вестись вручную ниже
		$this
			->addSkipColumn('image_large')
		;
		$primaryKeyName = $object->getPrimaryKeyName();

		if (!$object->$primaryKeyName)
		{
			foreach ($_GET as $getKey => $getValue) {
				if(isset($object->$getKey)) {
					$object->$getKey = $getValue;
				}
			}
		}

		parent::setObject($object);

		return $this;
	}

	protected function _prepareForm()
	{
		parent::_prepareForm();

		$object = $this->_object;

		$modelName = $object->getModelName();
		$primaryKeyName = $object->getPrimaryKeyName();

		$oMainTab = $this->getTab('main');
//		$oAdditionalTab = $this->getTab('additional');

		/** @var Admin_Form_Entity $typeField */
		$typeField = $this->getField($this->_object->getPrimaryKeyName());
		$typeField->readonly('readonly');

		if ($object->$primaryKeyName) {
			/** @var Admin_Form_Entity $typeField */
			$pathField = $this->getField('path');
			$pathField->format(
				array(
					'minlen' => array('value' => 1),
					'maxlen' => array('value' => 255)
				)
			);
		}
		$oMainTab
			->add($oMainRow1 = Admin_Form_Entity::factory('Div')->class('row'))
		;

		if(isset($this->_object->image_large)) {
			// Добавляем новое поле типа файл
			$oImageField = Admin_Form_Entity::factory('File');

			$oLargeFilePath = is_file($this->_object->getLargeFilePath())
				? $this->_object->getLargeFileHref()
				: '';

			$sFormPath = $this->_Admin_Form_Controller->getPath();

			$windowId = $this->_Admin_Form_Controller->getWindowId();

			$oImageField
				->style("width: 400px;")
				->name("image")
				->id("image")
				->largeImage(array(
					'max_width' => $this->_object->largeImageMaxWidth,
					'max_height' => $this->_object->largeImageMaxHeight,
					'path' => $oLargeFilePath,
					'show_params' => false,
					'delete_onclick' =>
						"$.adminLoad({path: '{$sFormPath}', additionalParams:
							'hostcms[checked][{$this->_datasetId}][{$this->_object->getPrimaryKey()}]=1', action: 'deleteLargeImage', windowId: '{$windowId}'}); return false",
					'caption' => Core::_($this->_object->getCurrentModelName().'.items_catalog_image'),
				))
				->smallImage(array('show' => FALSE))
			;

			$this->addField($oImageField);
			// Добавляем поле картинки группы товаров
			$oMainRow1->add($oImageField);
		}

		return $this;
	}

	protected function _applyObjectProperty()
	{
		$saveFlag = false;
		parent::_applyObjectProperty();

		$aCore_Config = Core::$mainConfig;

		if(isset($this->_object->image_large)) {
			$bLargeImageIsCorrect =
				// Поле файла большого изображения существует
				!is_null($aFileData = Core_Array::getFiles('image', NULL))
				// и передан файл
				&& intval($aFileData['size']) > 0;

			if ($bLargeImageIsCorrect)
			{
				// Проверка на допустимый тип файла
				if (Core_File::isValidExtension($aFileData['name'], $aCore_Config['availableExtension']))
				{
					// Удаление файла большого изображения
					if ($this->_object->image_large)
					{
						// !! дописать метод
						$this->_object->deleteLargeImage();
					}

					$file_name = $aFileData['name'];

					// Определяем расширение файла
					$ext = Core_File::getExtension($aFileData['name']);
					$large_image = $this->_object->getModelShortName() . '_' . $this->_object->getPrimaryKey() . '.' . $ext;
				}
				else
				{
					$this->addMessage(Core_Message::get(Core::_('Core.extension_does_not_allow', Core_File::getExtension($aFileData['name'])), 'error'));
				}

				// Путь к файлу-источнику большого изображения;
				$param['large_image_source'] = $aFileData['tmp_name'];
				// Оригинальное имя файла большого изображения
				$param['large_image_name'] = $aFileData['name'];
				$param['large_image_target'] = $this->_object->getItemPath() . $large_image;
				$param['create_small_image_from_large'] = false;
				$param['large_image_max_width'] = $this->_object->largeImageMaxWidth;
				$param['large_image_max_height'] = $this->_object->largeImageMaxHeight;
				$this->_object->createDir();

				$result = Core_File::adminUpload($param);

				if ($result['large_image'])
				{
					$this->_object->image_large = $large_image;
					$saveFlag = true;
				}

			}
		}

		$saveFlag && $this->_object->save();
	}

	protected function dropZone($id='dropzone', $title='DropZone', $type='Basebuy_Moto_Modification') {
		// Заголовок формы добавляется до вывода крошек, которые могут быть добавлены в контроллере
		$oAdmin_Form_Entity_Form_Div = Core::factory('Core_Html_Entity_Div')
			->class('form-group col-lg-6 col-md-6 col-sm-12 col-xs-12')
			->add(
				$oAdmin_Form_Entity_Form = Core::factory('Core_Html_Entity_Div')
					->id($id)
//					->name("Form_".$type)
//					->method('post')
//					->enctype('multipart/form-data')
					->class('dz-utils dropzone')
			)
		;
//		$oAdmin_Form_Entity_Form = Core::factory('Core_Html_Entity_Form')
//			->id($id)
//			->name("Form_".$type)
//			->method('post')
//			->enctype('multipart/form-data')
//			->class('dropzone');

//		$oAdmin_Form_Entity_Form
//			->add(
//				Core::factory('Core_Html_Entity_H3')
//					->class("before-pink")
//					->value($title)
//			)
//			->add(
//				$this->_addButtons()
//			);
		$fallbackLayer = Core::factory('Core_Html_Entity_Div');
		$fallbackLayer
			->class('fallback')
			->add( Core::factory('Core_Html_Entity_Code')->value('<input name="file".$id type="file" multiple="multiple" />') )
		;
		$oAdmin_Form_Entity_Form
			->add( Core::factory('Core_Html_Entity_Input')->type('hidden')->name('operation')->value('addItem') )
			->add( Core::factory('Core_Html_Entity_Input')->type('hidden')->name('galleryModel')->value($type) )
			->add( Core::factory('Core_Html_Entity_Input')->type('hidden')->name('itemID')->value($this->_object->getPrimaryKey()) )
			->add( $fallbackLayer )
		;
		return $oAdmin_Form_Entity_Form_Div;
	}
}