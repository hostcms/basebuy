<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Auto_Model
 *
 * @package HostCMS
 * @subpackage Auto
 * @version 6.x
 * @author art Борисов Михаил Юрьевич
 * @copyright © 2017 Борисов Михаил Юрьевич
 */
class Basebuy_Moto_Serie_Model extends Basebuy_Moto_Entity
{
	protected $_tableName = 'car_serie';

	/**
	 * Primary key
	 * @var string
	 */
	protected $_primaryKey = 'id_car_serie';

	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	/**
	 * One-to-many or many-to-many relations
	 * @var array
	 */
	protected $_hasMany = array(
		'moto_modification' => array(
			'foreign_key' => 'id_car_serie',
			'model' => 'basebuy_moto_modification',
		),
	);

	/**
	 * Belongs to relations
	 * @var array
	 */
	protected $_belongsTo = array(
		'moto_model' => array(
			'foreign_key' => 'id_car_model',
			'model' => 'basebuy_moto_model',
			'primary_key' => 'id_car_model',
		),
		'moto_type' => array(
			'foreign_key' => 'id_car_type',
			'model' => 'basebuy_moto_type',
			'primary_key' => 'id_car_type',
		),
	);

}