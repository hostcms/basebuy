<?php
/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
return array(
	'menu' => 'Каталог мототехники. Модификации',
	'header_admin_forms' => 'Действия с модификациями техники',
	'menu_add' => 'Добавить модификацию',
	'edit_title' => 'Редактировать модификацию',
	'add_title' => 'Добавить модификацию',
	'edit_success' => 'Выполнено успешно',
	//-- форма --
	'tab_gallery_title' => 'Изображения',
	'gallery_title' => 'Фотогалерея',
	'items_catalog_image' => 'Основное изображение',

	'name' => 'Наименование',
	'title' => 'Отображаемое название',
	'name_rus' => 'Наименование в единственном числе',
	'path' => 'Локальный путь',
);