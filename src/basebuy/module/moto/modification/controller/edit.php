<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
class Basebuy_Moto_Modification_Controller_Edit extends Basebuy_Moto_Controller_Edit
{

	protected function _prepareForm()
	{
		parent::_prepareForm();

		$object = $this->_object;

		$modelName = $object->getModelName();
		$primaryKeyName = $object->getPrimaryKeyName();

		$oMainTab = $this->getTab('main');
		$oAdditionalTab = $this->getTab('additional');

		// Создаем вкладки
		$oShopItemTabGalery = Admin_Form_Entity::factory('Tab')
			->caption(Core::_('Basebuy_Moto_Modification.tab_gallery_title'))
			->name('Gallery');

		$oShopItemTabGalery
			->add($oShopItemTabGaleryRow1 = Admin_Form_Entity::factory('Div')->class('row'));

		$fieldImage = $this->getField('image');

		$oMainTab
			->move($fieldImage, $oShopItemTabGaleryRow1);
		//-- DropZone --------------------------------------------------------------------------------------------------
		$dzFormDopImages = $this->dropZone('images', 'Дополнительные изображения', 'Basebuy_Moto_Modification');

		$oXslPanel = Core::factory('Core_Html_Entity_Div')
			->class('page-body')
			->add(
				Core::factory('Core_Html_Entity_H4')
					->class("row-title before-pink")
					->style("margin-left: -11px; padding: 5px 20px")
					->value(Core::_('Basebuy_Moto_Modification.gallery_title').' "'.$this->_object->gatFullName().'"')
			)
			->add(
				Core::factory('Core_Html_Entity_Div')
					->class('row')
					->add(
						Core::factory('Core_Html_Entity_Div')
							->class('col-md-12')
							->add(
								Core::factory('Core_Html_Entity_Div')
									->class('widget')
									->add(
										Core::factory('Core_Html_Entity_Div')
											->class('row widget-body')
											->add($dzFormDopImages)
									)
							)
					)
			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->value('if(typeof(Dropzone)!==\'undefined\')
								Dropzone.autoDiscover = false;
								initGalleries();')
			)
		;

			$oShopItemTabGaleryRow1->add($oXslPanel);


		// Добавляем вкладки
		$this
			->addTabAfter($oShopItemTabGalery, $oMainTab)
		;

		$title = $object->id_car_type
			? Core::_('Basebuy_Moto_Modification.edit_title')
			: Core::_('Basebuy_Moto_Modification.add_title');

		$this->title($title);

		return $this;
	}
}