<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');
/**
 * Module.
 *
 * @package HostCMS 6\Module
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © mikeborisov, m.u.borisov@gmail.com
 */
class Basebuy_Module extends Core_Module
{
	/**
	 * Module version
	 * @var string
	 */
	public $version = '0.1';
	/**
	 * Module date
	 * @var date
	 */
	public $date = '2017-08-16';

	protected $_afSettings = array(
		'basebuy_moto_type' => array(											//-- Форма
			'guid' => '2C98D19B-D4E0-2632-D766-25F61EDF61F7',		//-- guid формы
			'key_field' => 'id_car_type',							//-- Наименование ключевого поля из БД
			'default_order_field' => 'name',					//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 0,							//-- Направление сортировки: 0 - по возрастанию, 1 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Basebuy типы мототехники',					//-- по-русски - 1=идентификатор языка
				2 => 'Basebuy moto katalog'							//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id_car_type' => array(								//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
					'editable' => false,							//-- редактировать на форме
				),
				'path' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Путь',								//-- по-русски - 1=идентификатор языка
						2 => 'Path'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- поле сортировки
					'width' => '100px',
					'editable' => false,
					'type' => 10,
					'link' => '/admin/basebuy/moto/mark/index.php?id_car_type={id_car_type}',
					'onclick' => "$.adminLoad({path: '/admin/basebuy/moto/mark/index.php', additionalParams: 'id_car_type={id_car_type}', windowId: '{windowId}'}); return false",
				),
				'name' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Наименование',						//-- по-русски - 1=идентификатор языка
						2 => 'Name'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 30,								//-- поле сортировки
				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '',								//--
					'icon' => 'fa fa-pencil',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => 0,
					'confirm' => 0,
				),
			)
		),
		'basebuy_moto_mark' => array(											//-- Форма
			'guid' => '58251EAE-6D60-FCE0-6854-1084572FDBBB',		//-- guid формы
			'key_field' => 'id_car_mark',							//-- Наименование ключевого поля из БД
			'default_order_field' => 'name',					//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 0,							//-- Направление сортировки: 0 - по возрастанию, 1 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Basebuy марка мототехники',					//-- по-русски - 1=идентификатор языка
				2 => 'Basebuy moto mark'							//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id_car_mark' => array(								//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
					'editable' => false,							//-- редактировать на форме
				),
				'path' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Путь',								//-- по-русски - 1=идентификатор языка
						2 => 'Path'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- поле сортировки
					'width' => '100px',
					'editable' => false,
					'type' => 10,
					'link' => '/admin/basebuy/moto/model/index.php?id_car_type={id_car_type}&id_car_mark={id_car_mark}',
					'onclick' => "$.adminLoad({path: '/admin/basebuy/moto/model/index.php', additionalParams: 'id_car_type={id_car_type}&id_car_mark={id_car_mark}', windowId: '{windowId}'}); return false",
				),
				'name' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Наименование',						//-- по-русски - 1=идентификатор языка
						2 => 'Name'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 30,								//-- поле сортировки
				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '',								//--
					'icon' => 'fa fa-pencil',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => 0,
					'confirm' => 0,
				),
				'deleteLargeImage' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Удаление большого изображения',						//-- по-русски - 1=идентификатор языка
						2 => 'Deleting large Image'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- сортировка для действий
				),
			)
		),
		'basebuy_moto_model' => array(											//-- Форма
			'guid' => 'F2E25661-9858-752A-B29A-1FFE434A9502',		//-- guid формы
			'key_field' => 'id_car_model',							//-- Наименование ключевого поля из БД
			'default_order_field' => 'name',					//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 0,							//-- Направление сортировки: 0 - по возрастанию, 1 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Basebuy модель мототехники',					//-- по-русски - 1=идентификатор языка
				2 => 'Basebuy moto model'							//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id_car_model' => array(								//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
					'editable' => false,							//-- редактировать на форме
				),
				'path' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Путь',								//-- по-русски - 1=идентификатор языка
						2 => 'Path'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- поле сортировки
					'width' => '100px',
					'editable' => false,
					'type' => 10,
					'link' => '/admin/basebuy/moto/modification/index.php?id_car_type={id_car_type}&id_car_mark={id_car_mark}&id_car_model={id_car_model}',
					'onclick' => "$.adminLoad({path: '/admin/basebuy/moto/modification/index.php', additionalParams: 'id_car_type={id_car_type}&id_car_mark={id_car_mark}&id_car_model={id_car_model}', windowId: '{windowId}'}); return false",
				),
				'name' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Наименование',						//-- по-русски - 1=идентификатор языка
						2 => 'Name'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 30,								//-- поле сортировки
				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '',								//--
					'icon' => 'fa fa-pencil',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => 0,
					'confirm' => 0,
				),
				'deleteLargeImage' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Удаление большого изображения',						//-- по-русски - 1=идентификатор языка
						2 => 'Deleting large Image'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- сортировка для действий
				),
			)
		),
		'basebuy_moto_modification' => array(											//-- Форма
			'guid' => '5ED3EF45-813A-7AA4-8052-6B256D1BAEE3',		//-- guid формы
			'key_field' => 'id_car_modification',					//-- Наименование ключевого поля из БД
			'default_order_field' => 'name',						//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 0,							//-- Направление сортировки: 0 - по возрастанию, 1 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Basebuy модификация мототехники',				//-- по-русски - 1=идентификатор языка
				2 => 'Basebuy moto model'							//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id_car_modification' => array(								//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
					'editable' => false,							//-- редактировать на форме
				),
				'name' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Наименование',						//-- по-русски - 1=идентификатор языка
						2 => 'Name'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 30,								//-- поле сортировки
				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '',								//--
					'icon' => 'fa fa-pencil',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => 0,
					'confirm' => 0,
				),
				'deleteLargeImage' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Удаление большого изображения',						//-- по-русски - 1=идентификатор языка
						2 => 'Deleting large Image'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- сортировка для действий
				),
			)
		),
	);

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!is_null($user=Core_Entity::factory('User')->getCurrent()) && $user->id > 0) {
//			Core_Skin::instance()->addCss('/admin/basebuy/css/dropzone.css');
//			Core_Skin::instance()->addCss('/admin/basebuy/css/basebuy.css');
//			Core_Skin::instance()->addJs('/admin/basebuy/js/basebuy.js');
		}

		$this->menu = array(
		array(
			'sorting' => 40,
			'block' => 0,
			'ico' => 'fa fa-motorcycle',
			'name' => Core::_('Basebuy_Moto.menu'),
			'href' => "/admin/basebuy/moto/type/index.php",
			'onclick' => "$.adminLoad({path: '/admin/basebuy/moto/type/index.php'}); return false"
		)
		);
	}

	/**
	 * Install module.
	 */
	public function install() {
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			//-- Добавляем форму --
			$adminForm = $this->adminForm($afSetting['name'], $afSetting);
		}
	}

	/**
	 * Uninstall.
	 */
	public function uninstall()
	{
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (!is_null($oAdmin_Form)) {
				// Удаление формы
				$oAdmin_Form->delete();
			}
		}
	}

	protected function adminForm($afName, $afSetting) {
		$oAdmin_Form = NULL;
		if(isset($afSetting['guid'])) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (is_null($oAdmin_Form)) {
				$oAdmin_Word_Form = $this->adminWordForm($afName);
				$oAdmin_Form = Core_Entity::factory('Admin_Form');
				$oAdmin_Form->admin_word_id = $oAdmin_Word_Form->id;
				$oAdmin_Form->key_field = $afSetting['key_field'];
				$oAdmin_Form->default_order_field = Core_Array::get($afSetting, 'default_order_field', $afSetting['key_field']);

				$oAdmin_Form->on_page = Core_Array::get($afSetting, 'on_page', 20);
				$oAdmin_Form->show_operations = Core_Array::get($afSetting, 'show_operations', 1);
				$oAdmin_Form->show_group_operations = Core_Array::get($afSetting, 'show_group_operations', 0);
				$oAdmin_Form->default_order_direction = Core_Array::get($afSetting, 'default_order_direction', 0);
				$oAdmin_Form->guid = $afSetting['guid'];
				$oAdmin_Form->save();
			}
			if(isset($afSetting['fields']) && is_array($afSetting['fields'])) {
				//-- Добавляем поля в форму --
				foreach ($afSetting['fields'] as $dbFieldKey => $field) {
					$adminFormField = $this->adminFormField($dbFieldKey, $field);
					$oAdmin_Form->add($adminFormField);
				}
			}
			if(isset($afSetting['actions']) && is_array($afSetting['actions'])) {
				//-- Добавляем действия в форму --
				foreach ($afSetting['actions'] as $dbActionKey => $action) {
					$adminFormAction = $this->adminFormAction($dbActionKey, $action);
					$oAdmin_Form->add($adminFormAction);
				}
			}
		}

		return $oAdmin_Form;
	}

	protected function adminFormAction($fDbName, $action) {
		$oAdmin_Word_Form = $this->adminWordForm($action['name']);
		$oAdmin_Form_Action = Core_Entity::factory('Admin_Form_Action')->save();
		$oAdmin_Form_Action->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Action->name = $fDbName;
		$oAdmin_Form_Action->picture = Core_Array::get($action, 'picture', '');
		$oAdmin_Form_Action->icon = Core_Array::get($action, 'icon', '');
		$oAdmin_Form_Action->color = Core_Array::get($action, 'color', '');
		$oAdmin_Form_Action->single = Core_Array::get($action, 'single', 0);
		$oAdmin_Form_Action->group = Core_Array::get($action, 'group', 0);
		$oAdmin_Form_Action->sorting = Core_Array::get($action, 'sorting', 1000);
		$oAdmin_Form_Action->dataset = Core_Array::get($action, 'dataset', '-1');
		$oAdmin_Form_Action->confirm = Core_Array::get($action, 'confirm', 0);
//		$oAdmin_Form_Action->save();
		return $oAdmin_Form_Action;
	}

	protected function adminFormField($fDbName, $field) {
		$oAdmin_Word_Form = $this->adminWordForm($field['name']);
		$oAdmin_Form_Field = Core_Entity::factory('Admin_Form_Field')->save();
		$oAdmin_Form_Field->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Field->name = $fDbName;
		$oAdmin_Form_Field->sorting = Core_Array::get($field, 'sorting', 1000);
		$oAdmin_Form_Field->ico = Core_Array::get($field, 'ico', '');
		$oAdmin_Form_Field->type = Core_Array::get($field, 'type', 1);
		$oAdmin_Form_Field->format = Core_Array::get($field, 'format', '');
		$oAdmin_Form_Field->allow_sorting = Core_Array::get($field, 'allow_sorting', 1);
		$oAdmin_Form_Field->allow_filter = Core_Array::get($field, 'allow_filter', 1);
		$oAdmin_Form_Field->editable = Core_Array::get($field, 'editable', 1);
		$oAdmin_Form_Field->filter_type = Core_Array::get($field, 'filter_type', 0);
		$oAdmin_Form_Field->class = Core_Array::get($field, 'class', '');
		$oAdmin_Form_Field->width = Core_Array::get($field, 'width', '');
		$oAdmin_Form_Field->image = Core_Array::get($field, 'image', '');
		$oAdmin_Form_Field->link = Core_Array::get($field, 'link', '');
		$oAdmin_Form_Field->onclick = Core_Array::get($field, 'onclick', '');
		$oAdmin_Form_Field->list = Core_Array::get($field, 'list', '');
		return $oAdmin_Form_Field;
	}

	protected function adminWordForm($afName) {
		$oAdmin_Word_Form = Core_Entity::factory('Admin_Word')->save();
		foreach ($afName as $langID => $langName) {
			$oAdmin_Word_Value = $this->adminWordValue($langID, $langName);
			$oAdmin_Word_Form->add($oAdmin_Word_Value);
		}
		return $oAdmin_Word_Form;
	}

	protected function adminWordValue($language_id, $name) {
		$oAdmin_Word_Value = Core_Entity::factory('Admin_Word_Value')->save();
		$oAdmin_Word_Value->admin_language_id = $language_id;
		$oAdmin_Word_Value->name = $name;
		$oAdmin_Word_Value->save();
		return $oAdmin_Word_Value;
	}
}