<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Basebuy_Observers_Core {
	static $_utilsAllowedProperties = array();

	static public function onBeforeShowCss($object, $args) {
		$instance = Core_Page::instance();
		if($instance->user != FALSE) {
			$instance->css('/admin/basebuy/css/basebuy.css');
		}
	}

	static public function onBeforeShowJs($object, $args) {
		$instance = Core_Page::instance();
		if($instance->user != FALSE) {
			$instance->js('/admin/basebuy/js/basebuy.js');
		}
	}
}