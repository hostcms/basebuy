<?php
/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
require_once(__DIR__.'/../../../../../../../../bootstrap.php');

Core_Auth::authorization($sModule = 'basebuy');

// Код формы
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid('2C98D19B-D4E0-2632-D766-25F61EDF61F7');
$iAdmin_Form_Id = $oAdmin_Form->id;
$sAdminFormAction = '/admin/basebuy/moto/type/index.php';

$oAdmin_Form = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id);

// Контроллер формы
$oAdmin_Form_Controller = Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->module(Core_Module::factory($sModule))
	->setUp()
	->path($sAdminFormAction)
	->title(Core::_('Basebuy_Moto_Type.menu'))
	->pageTitle(Core::_('Basebuy_Moto_Type.menu'));

// Меню формы
$oAdmin_Form_Entity_Menus = Admin_Form_Entity::factory('Menus');

// Элементы меню
$oAdmin_Form_Entity_Menus->add(
	Admin_Form_Entity::factory('Menu')
		->name(Core::_('Basebuy_Moto_Type.header_admin_forms'))
		->icon('fa fa-motorcycle')
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Basebuy_Moto_Type.menu_add'))
				->icon('fa fa-plus')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref($oAdmin_Form_Controller->getPath(), 'edit', NULL, 0, 0)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax($oAdmin_Form_Controller->getPath(), 'edit', NULL, 0, 0)
				)
		)
);

// Добавляем все меню контроллеру
$oAdmin_Form_Controller->addEntity($oAdmin_Form_Entity_Menus);

// Представитель класса хлебных крошек
$oAdmin_Form_Entity_Breadcrumbs = Admin_Form_Entity::factory('Breadcrumbs');

// Создаем первую хлебную крошку
$oAdmin_Form_Entity_Breadcrumbs->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name(Core::_('Basebuy_Moto.menu'))
		->href(
			$oAdmin_Form_Controller->getAdminLoadHref($oAdmin_Form_Controller->getPath(), NULL, NULL, '')
		)
		->onclick(
			$oAdmin_Form_Controller->getAdminLoadAjax($oAdmin_Form_Controller->getPath(), NULL, NULL, '')
		)
);

// Добавляем все крошки контроллеру
$oAdmin_Form_Controller->addEntity($oAdmin_Form_Entity_Breadcrumbs);


// Действие редактирования
$oAdmin_Form_Action = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('edit');

if ($oAdmin_Form_Action && $oAdmin_Form_Controller->getAction() == 'edit')
{
	$Auto_Controller_Edit = Admin_Form_Action_Controller::factory(
		'Basebuy_Moto_Type_Controller_Edit', $oAdmin_Form_Action
	);

	// Хлебные крошки для контроллера редактирования
	$Auto_Controller_Edit
		->addEntity(
			$oAdmin_Form_Entity_Breadcrumbs
		);

	// Добавляем типовой контроллер редактирования контроллеру формы
	$oAdmin_Form_Controller->addAction($Auto_Controller_Edit);
}

//// Действие "Применить"
//$oAdminFormActionApply = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
//	->Admin_Form_Actions
//	->getByName('apply');
//
//if ($oAdminFormActionApply && $oAdmin_Form_Controller->getAction() == 'apply')
//{
//	$oControllerApply = Admin_Form_Action_Controller::factory(
//		'Admin_Form_Action_Controller_Type_Apply', $oAdminFormActionApply
//	);
//
//	// Добавляем типовой контроллер редактирования контроллеру формы
//	$oAdmin_Form_Controller->addAction($oControllerApply);
//}
//
//// Действие "Копировать"
//$oAdminFormActionCopy = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
//	->Admin_Form_Actions
//	->getByName('copy');
//
//if ($oAdminFormActionCopy && $oAdmin_Form_Controller->getAction() == 'copy')
//{
//	$oControllerCopy = Admin_Form_Action_Controller::factory(
//		'Admin_Form_Action_Controller_Type_Copy', $oAdminFormActionCopy
//	);
//
//	// Добавляем типовой контроллер редактирования контроллеру формы
//	$oAdmin_Form_Controller->addAction($oControllerCopy);
//}
//
// Источник данных 0
$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(
	Core_Entity::factory('Basebuy_Moto_Type')
);

//// Ограничение источника 0 по родительской группе
//$oAdmin_Form_Dataset->addCondition(
//		array(
//			'where' => array('site_id', '=', CURRENT_SITE)
//		)
//	)
//	->changeField('name', 'link', '/admin/auto/group/index.php?auto_id={id}')
//	->changeField('name', 'onclick', "$.adminLoad({path: '/admin/auto/group/index.php', additionalParams: 'auto_id={id}', windowId: '{windowId}'}); return false");

// Добавляем источник данных контроллеру формы
$oAdmin_Form_Controller->addDataset(
	$oAdmin_Form_Dataset
);

//// Действие "Удалить файл watermark"
//$oAdminFormActionDeleteWatermarkFile = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
//	->Admin_Form_Actions
//	->getByName('deleteWatermarkFile');
//
//if ($oAdminFormActionDeleteWatermarkFile && $oAdmin_Form_Controller->getAction() == 'deleteWatermarkFile')
//{
//	$oAutoControllerDeleteWatermarkFile = Admin_Form_Action_Controller::factory(
//		'Admin_Form_Action_Controller_Type_Delete_File', $oAdminFormActionDeleteWatermarkFile
//	);
//
//	$oAutoControllerDeleteWatermarkFile
//		->methodName('deleteWatermarkFile')
//		->divId(array('preview_large_watermark_file', 'delete_large_watermark_file'));
//
//	// Добавляем контроллер редактирования контроллеру формы
//	$oAdmin_Form_Controller->addAction($oAutoControllerDeleteWatermarkFile);
//}

// Показ формы
$oAdmin_Form_Controller->execute();