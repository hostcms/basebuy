<?
/**
 * Online moto.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © m.u.borisov@gmail.com
 */
require_once(__DIR__.'/../../../../../../../../bootstrap.php');

Core_Auth::authorization($sModule = 'basebuy');

$id_car_type = intval(Core_Array::getGet('id_car_type', 0));
$id_car_mark = intval(Core_Array::getGet('id_car_mark', 0));
$oMotoType = Core_Entity::factory('Basebuy_Moto_Type', $id_car_type);
$oMotoMark = Core_Entity::factory('Basebuy_Moto_Mark', $id_car_mark);
// Код формы
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid('F2E25661-9858-752A-B29A-1FFE434A9502');
$iAdmin_Form_Id = $oAdmin_Form->id;
$sAdminFormAction = '/admin/basebuy/moto/model/index.php';

$oAdmin_Form = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id);

// Контроллер формы
$oAdmin_Form_Controller = Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->module(Core_Module::factory($sModule))
	->setUp()
	->path($sAdminFormAction)
	->title(Core::_('Basebuy_Moto_Model.menu'))
	->pageTitle(Core::_('Basebuy_Moto_Model.menu'));

// Меню формы
$oAdmin_Form_Entity_Menus = Admin_Form_Entity::factory('Menus');

// Элементы меню
$oAdmin_Form_Entity_Menus->add(
	Admin_Form_Entity::factory('Menu')
		->name(Core::_('Basebuy_Moto_Model.header_admin_forms'))
		->icon('fa fa-motorcycle')
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Basebuy_Moto_Model.menu_add'))
				->icon('fa fa-plus')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref($oAdmin_Form_Controller->getPath(), 'edit', NULL, 0, 0)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax($oAdmin_Form_Controller->getPath(), 'edit', NULL, 0, 0)
				)
		)
);

// Добавляем все меню контроллеру
$oAdmin_Form_Controller->addEntity($oAdmin_Form_Entity_Menus);

// Представитель класса хлебных крошек
$oAdmin_Form_Entity_Breadcrumbs = Admin_Form_Entity::factory('Breadcrumbs');

// Создаем первую хлебную крошку
$oAdmin_Form_Entity_Breadcrumbs->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name(Core::_('Basebuy_Moto.menu'))
		->href(
			$oAdmin_Form_Controller->getAdminLoadHref('/admin/basebuy/moto/type/index.php', NULL, NULL, '')
		)
		->onclick(
			$oAdmin_Form_Controller->getAdminLoadAjax('/admin/basebuy/moto/type/index.php', NULL, NULL, '')
		)
)->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name($oMotoType->name)
		->href(
			$oAdmin_Form_Controller->getAdminLoadHref('/admin/basebuy/moto/mark/index.php', NULL, NULL, "id_car_type={$id_car_type}")
		)
		->onclick(
			$oAdmin_Form_Controller->getAdminLoadAjax('/admin/basebuy/moto/mark/index.php', NULL, NULL, "id_car_type={$id_car_type}")
		)
)->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name($oMotoMark->name)
		->href(
			$oAdmin_Form_Controller->getAdminLoadHref($oAdmin_Form_Controller->getPath(), NULL, NULL, "id_car_type={$id_car_type}&id_car_mark={$id_car_mark}")
		)
		->onclick(
			$oAdmin_Form_Controller->getAdminLoadAjax($oAdmin_Form_Controller->getPath(), NULL, NULL, "id_car_type={$id_car_type}&id_car_mark={$id_car_mark}")
		)
);

// Добавляем все крошки контроллеру
$oAdmin_Form_Controller->addEntity($oAdmin_Form_Entity_Breadcrumbs);

// Действие редактирования
$oAdmin_Form_Action = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('edit');

if ($oAdmin_Form_Action && $oAdmin_Form_Controller->getAction() == 'edit')
{
	$Auto_Controller_Edit = Admin_Form_Action_Controller::factory(
		'Basebuy_Moto_Model_Controller_Edit', $oAdmin_Form_Action
	);

	// Хлебные крошки для контроллера редактирования
	$Auto_Controller_Edit
		->addEntity(
			$oAdmin_Form_Entity_Breadcrumbs
		);

	// Добавляем типовой контроллер редактирования контроллеру формы
	$oAdmin_Form_Controller->addAction($Auto_Controller_Edit);
}

// Действие "Удаление файла большого изображения"
$oAction = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('deleteLargeImage');

if ($oAction && $oAdmin_Form_Controller->getAction() == 'deleteLargeImage')
{
	$oDeleteLargeImageController = Admin_Form_Action_Controller::factory(
		'Admin_Form_Action_Controller_Type_Delete_File', $oAction
	);

	$oDeleteLargeImageController
		->methodName('deleteLargeImage')
		->divId(array('preview_large_image', 'delete_large_image'));

	// Добавляем контроллер удаления изображения к контроллеру формы
	$oAdmin_Form_Controller->addAction($oDeleteLargeImageController);
}

// Источник данных 0
$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(
	Core_Entity::factory('Basebuy_Moto_Model')
);
$oAdmin_Form_Dataset->addCondition(
	array(
		'where' => array('id_car_type', '=', $id_car_type),
		'where' => array('id_car_mark', '=', $id_car_mark),
	)
);

// Добавляем источник данных контроллеру формы
$oAdmin_Form_Controller->addDataset(
	$oAdmin_Form_Dataset
);

// Показ формы
$oAdmin_Form_Controller->execute();