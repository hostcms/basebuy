function initGalleries() {
	$.each($(".dropzone"), function(index, dzone) {
		var galleryType = $(this).find("input[name='galleryType']");
		var itemID = $(this).find("input[name='itemID']");
		var galleryModel = $(this).find("input[name='galleryModel']");

// console.log(dzone);
		$(dzone).dropzone({
			dictDefaultMessage: "Перенести файлы из проводника, или кликни по области",
			addRemoveLinks: true,
			clickable: true,
			url: "/admin/basebuy/gallery/process.php",
			success: function(file,status){
				// console.log(status);
				// console.log(status.status);
				if(status.status == "OK") {
					var _thisGrid = $(file.previewElement).parents('.dropzone').find('.sortable');
					file.previewElement.className+=" dz-success";
					var $previewImg = $(file.previewElement).find('.dz-image img'),
						$previewDetails = $(file.previewElement).find('.dz-details');
					_thisGrid.append($(file.previewElement));
					$(file._removeLink).attr('data-itemID',status.itemID);
					$previewDetails.append($previewImg);
					$previewDetails.append(
						$("<input/>", {class:"dz-checkbox", type:"checkbox", 'data-itemID': status.itemID})
					);
				}else {
					file.previewElement.className+=" dz-error";
					file.previewElement.children[4].firstChild.innerHTML = status.message;
				}
			},
			removedfile: function(file){
				createModalWindow($(file._removeLink), "Удалить файл?", "single");
				removeSingleClick = $(file._removeLink);
			},
			init: function(){
				var dataSend = {
					"operation": 'getList',
					"galleryType": galleryType.val(),
					"itemID": itemID.val(),
					"class": galleryModel.val()
				};
				var vDropZone = $(this.element);
				vDropZone.append($("<div/>",{class:"sortable"}));
				var vGridster = vDropZone.find('.sortable');
				// console.log(dataSend);
				$.ajax({
					type: "POST",
					url: "/admin/basebuy/gallery/process.php",
					data: dataSend,
					dataType: "json",
					success: function(data){
						$.each(data, function(index,value){
							vGridster.append(createGalleryElement(value));
						});
					},
					error: function(error){
						console.log(error);
						console.log('init error');
					}
				});
			},
			error: function(arg1) {
				console.log(arg1);
			}
		});
	});
}


var removeAllClick,
	$body = $('body');

$(document).on('click','input[name=deleteMarked]',function(){
	createModalWindow($(this), "Удалить выделенные файлы?", "all");
	removeAllClick = $(this);
});

$(document).on('click','input[name=saveMainImages]',function(){
	console.log('Save');
	var __button = $(this),
		__form = $(this).closest('form'),
		__itemID = $(this).attr('data-item-id'),
		__formData = new FormData(__form[0]);

	__button.attr('disabled', 'disabled');
	// console.log(__formData);
	$.ajax({
		type: "POST",
		url: "/admin/basebuy/gallery/process.php?itemID="+__itemID+'&operation=saveMainImage',
		data: __formData,
		async: false,
		cache: false,
		contentType: false,
		processData: false,
		// contentType: 'multipart/form-data',
		// enctype: 'multipart/form-data',
		// dataType: 'html',
		dataType: "json",
		success: function(data){
			__button.removeAttr('disabled');
			if(data.status == "OK"){
				// removeSingleClick.parents('.dz-preview').remove();
			}
			else {
				// removeSingleClick.siblings('.dz-error-message').text(data.message).parent().removeClass('dz-success').addClass('dz-error');
			}
		},
		error: function(){
			__button.removeAttr('disabled');
			console.log('ajax error');
		},
		complete: function(){
			__button.removeAttr('disabled');
			// _overlay.remove();
		}
	});
});

var removeSingleClick;
$(document).on('click','.dz-remove',function(){
	createModalWindow($(this), "Удалить файл?", "single");
	removeSingleClick = $(this);
});

$(document).on('click','.modalForm_ans',function(){
	var _overlay = $('.overlayForm');
	var _type = $(this).attr('data-type');

	if( $(this).attr('id') == "ansY" ){
		switch(_type) {

			//удаление одного элемента
			case 'single':
				$.ajax({
					type: "POST",
					url: "/admin/basebuy/gallery/process.php",
					data: {'operation': 'deleteItem', 'itemID': removeSingleClick.attr('data-itemid'), 'galleryType': removeSingleClick.parents('.dropzone').attr('id')},
					dataType: "json",
					success: function(data){
						if(data.status == "OK"){
							removeSingleClick.parents('.dz-preview').remove();
						}
						else {
							removeSingleClick.siblings('.dz-error-message').text(data.message).parent().removeClass('dz-success').addClass('dz-error');
						}
					},
					error: function(){
						console.log('ajax error');
					},
					complete: function(){
						_overlay.remove();
					}
				});
				break;

			//удаление выделенных элементов
			case 'all':
				var checkedItems = removeAllClick.parents('.dropzone').find('.dz-checkbox:checked');
				var arrayID = [];
				checkedItems.each(function(){
					var _thisID = $(this).attr('data-itemid');
					arrayID.push(_thisID);
				});
				var dataSend = {
					"operation": 'deleteItems',
					"itemIDs": arrayID,
					"galleryType": removeAllClick.parents('.dropzone').attr('id')
				};
				$.ajax({
					type: "POST",
					url: "/admin/basebuy/gallery/process.php",
					data: dataSend,
					dataType: "json",
					success: function(data){
						if(data.status == "OK"){
							$.each(data.deletedItems, function(index,value){
								$('.dz-checkbox[data-itemid='+value+']').parents('.dz-preview').remove();
							});
						}else {
							$.each(data.deletedItems, function(index,value){
								$('.dz-checkbox[data-itemid='+value+']').parents('.dz-preview').remove();
							});
							$.each(data.unDeletedItems, function(index,value){
								$('.dz-checkbox[data-itemid='+value+']').parent().siblings('.dz-error-message').text(data.message).parent().removeClass('dz-success').addClass('dz-error');
							});
						}
					},
					error: function(){
						console.log('ajax error');
					},
					complete: function(){
						_overlay.remove();
					}
				});
				break;
		} //end switch

	} else {
		_overlay.remove();
	}
});

$(document).on('click','input[name=markAll]',function(){
	var _this = $(this),
		_allCheckbox = _this.parents('.dropzone').find('.dz-checkbox');
	//_allCheckbox.attr('checked','checked');
	_allCheckbox.each(function(){
		//$(this).attr('checked','checked');
		this.checked = true;
		highlightCheckedElement($(this));
	});
});

$(document).on('click','input[name=unMarkAll]',function(){
	var _this = $(this),
		_allCheckbox = _this.parents('.dropzone').find('.dz-checkbox');
	_allCheckbox.removeAttr('checked');
	_allCheckbox.each(function(){
		highlightCheckedElement($(this));
	});
});

$(document).on('change','.dz-checkbox', function(){
	highlightCheckedElement($(this));
});


//подсветка элемента
function highlightCheckedElement(_this){
	if(_this[0].checked) {
		_this.parents('.dz-preview').addClass('checked');
	}else {
		_this.parents('.dz-preview').removeClass('checked');
	}
}

//создание модального окна с затемнением
function createModalWindow(_this, _text, _type){
	_this.parents('body').append(
		$("<div/>",{class:"overlayForm"}).append(
			$("<div/>",{class:"modalForm"}).append(
				$("<div/>",{class:"modalForm_title", html:_text}),
				$("<a/>",{class:"modalForm_ans", id:"ansY", "data-type": _type, href:"javascript:undefined;", html:"Ok"}),
				$("<a/>",{class:"modalForm_ans", id:"ansN", "data-type": _type, href:"javascript:undefined;", html:"Отмена"})
			)
		)
	);
}

//создание элементов в галереи
function createGalleryElement(value){
	var layer = $('<div/>', {
		class: "dz-preview dz-processing dz-image-preview dz-success"
	}).append(
		$("<div/>", {
			class: "dz-details"
		}).append(
			$("<div/>", {class: "dz-filename"}).append( $("<span/>",{html:value.name}) ),
			$("<div/>", {class: "dz-size",html:" KiB"}).prepend( $("<strong/>",{html:value.size}) ),
			$("<img/>", {src:value.src, alt:value.name}),
			$("<input/>", {class:"dz-checkbox", type:"checkbox", 'data-itemID': value.itemID})
		)
	).append(
		$("<div/>", {class: "dz-success-mark"}),
		$("<div/>", {class: "dz-error-mark"}),
		$("<div/>", {class: "dz-error-message"}),
		$("<a/>",   {class: "dz-remove", href:"javascript:undefined;", 'data-itemID': value.itemID})
	);

	return layer;
}

