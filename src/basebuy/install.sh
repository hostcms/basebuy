#!/bin/bash

_CURRENTPATH=`pwd`

echo $_CURRENTPATH

if [ ! -d ../../../../../modules/basebuy ]; then 
    ln -s ../vendor/mikeborisov/hostcms-basebuy/src/basebuy/module ../../../../../modules/basebuy;
fi

if [ ! -d ../../../../../admin/basebuy ]; then 
    ln -s ../vendor/mikeborisov/hostcms-basebuy/src/basebuy/admin ../../../../../admin/basebuy;
fi

if [ ! -d ../../../../../modules/skin/bootstrap/module/basebuy ]; then
    ln -s ../../../../vendor/mikeborisov/hostcms-basebuy/src/basebuy/skin ../../../../../modules/skin/bootstrap/module/basebuy;
fi